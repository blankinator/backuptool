# Backup Tool

## Description

TODO

## Setup

### Requirements

- Linux or Unix based OS
- rsync
- Python 3.10+
- Install requirements with `pip install -r requirements.txt`

### Configuration

Create config file `config.json` in the root directory of the project. See `config.json.example` for an example.
Unfortunately, using a key file with passphrase is not supported yet.

Make sure to establish a connection to the remote server first, to add the host fingerprint!

## Usage

