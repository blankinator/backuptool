import json
import sys
import argparse
import traceback
from functools import partial

from backup_tool.backup.operator import run_backup
from backup_tool.utils.config_utils import load_config
from backup_tool.utils.mail_notifications import send_notification, get_mail_config, send_email
from backup_tool.utils.logging import get_logger

logger = get_logger(__name__)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Execute a backup with the given name from the config file.')
    parser.add_argument("--config_file", type=str, help="path to the config file", default="./config.json")
    parser.add_argument("backup_names", type=str, help="names of the backup to run, comma separated")
    parser.add_argument('--no_notifications', type=bool, help='set to true to disable notifications')
    args = parser.parse_args()

    # load config
    config = load_config(args.config_file)

    backups_to_run = [x.strip() for x in args.backup_names.split(',')]
    logger.info(f"Running backups: {backups_to_run}")

    # get mail config
    try:
        mail_config = get_mail_config(config)
        send_mail_function = partial(send_email,
                                     smtp_server=mail_config['smtp_server'],
                                     smtp_port=mail_config['smtp_port'],
                                     username=mail_config['username'],
                                     password=mail_config['password'],
                                     sender_address=mail_config['sender_address'])
    except Exception as e:
        logger.error(f'Error while reading mail config: {e}')
        sys.exit(1)

    successful_backups = []
    failed_backups = []
    backup_stats = dict()
    for backup_name in backups_to_run:
        try:
            backup_stat = run_backup(backup_name, config)
            successful_backups.append(backup_name)
        except Exception as e:
            logger.error(f"Error while running backup {backup_name}: {e}")
            backup_stat = {"error": str(e),
                           "stacktrace": traceback.format_exc()}
            failed_backups.append(backup_name)
        finally:
            # add backup stat for backup to the backup stats
            if backup_name not in backup_stats:
                backup_stats[backup_name] = backup_stat
            else:
                logger.warning(f"Backup {backup_name} already in backup stats")
                backup_stats[f"{backup_name}_2"] = backup_stat

    if not args.no_notifications:
        subject = f"Backup run: {len(successful_backups)} successful, {len(failed_backups)} failed"
        content = f"{subject}, \n\n" \
                  f"successful backups: {', '.join(successful_backups)}\n\n" \
                  f"failed backups: {', '.join(failed_backups)}\n\n" \
                  f"backup stats: {json.dumps(backup_stats, indent=4)}"
        send_notification(subject=subject,
                          content=content,
                          recipient_addresses=mail_config['recipients'],
                          send_mail_function=send_mail_function)
