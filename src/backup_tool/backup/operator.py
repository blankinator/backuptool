import os.path
from datetime import datetime

from backup_tool.utils.config_utils import load_config, get_backup_config
from backup_tool.utils.remote_utils import *
from backup_tool.utils.parsing import bytes_to_readable, bytes_per_second_to_readable
from backup_tool.utils.logging import get_logger

logger = get_logger(__name__)


def run_backup(backup_name: str,
               config: dict) -> dict:
    """
    Runs the backup identified by the given name in the config file
    :param backup_name: name of the backup to run
    :param config: configuration dict for backup tool
    :return: backup statistics as dict
    """

    # select the backup config with the given name
    current_backup_config = get_backup_config(backup_name, config)

    # iterate over all backup locations
    if current_backup_config["type"] == "network":
        backup_stats = do_remote_backup(current_backup_config)
    else:
        # TODO: implement more backup types
        raise NotImplementedError(f"Backup type {current_backup_config['type']} not (yet) implemented")

    return backup_stats


def do_remote_backup(backup_config: dict) -> dict:
    """
    Run a remote backup with the given name from the config file
    :param backup_config: configuration dict for backup tool
    :return: backup statistics as dict
    """
    current_start_time = datetime.now()

    # load config
    backup_locations = backup_config["backup_locations"]
    keyfile = backup_config["keyfile"]

    # initialize backup stats
    backup_stats = dict()

    # backup all locations
    for backup_location in backup_locations:
        # get required parameters
        try:
            source_directory = backup_location["source_directory"]
            # make sure source directory ends with a slash
            if not source_directory.endswith("/"):
                source_directory += "/"
            source_root = get_path_root(source_directory)
            destination_directory = backup_location["destination_directory"]
            destination_root = get_destination_root(destination_directory, source_root)
        except KeyError as e:
            raise KeyError(f"Key {e} not found in config file")

        # get optional parameters
        included_sub_nodes = backup_location[
            "included_sub_nodes"] if "included_sub_nodes" in backup_location else None
        update_only = backup_location["update_only"] if "update_only" in backup_location else False
        overwrite = backup_location["overwrite"] if "overwrite" in backup_location else False
        delete_old_files = backup_location["delete_old_files"] if "delete_old_files" in backup_location else False

        backup_location_stats = dict()
        backup_location_stats["included_sub_nodes"] = included_sub_nodes if included_sub_nodes else "all"

        # log config
        logger.info(f"Backing up {source_directory} to {destination_directory}")
        logger.info(f"Source root: {source_root}")
        logger.info(f"Destination root: {destination_root}")
        logger.info(f"Included sub nodes: {included_sub_nodes}")
        logger.info(f"Update only: {update_only}")
        logger.info(f"Overwrite: {overwrite}")
        logger.info(f"Delete old files: {delete_old_files}")

        # check if the source directory exists
        source_dir_exists = check_if_directory_exists(directory_url=source_directory,
                                                      keyfile_path=keyfile)
        source_is_file = check_if_file_exists(file_url=source_directory, keyfile_path=keyfile)

        # also check if the source directory is not a  file
        if not source_dir_exists:
            raise FileNotFoundError(f"Source directory {source_directory} does not exist")

        # check, if destination directory already exists
        destination_dir_exists = check_if_directory_exists(directory_url=destination_directory,
                                                           keyfile_path=keyfile)

        # create destination directory, if it does not exist
        if not destination_dir_exists:
            logger.info(f"Destination directory {destination_directory} does not exist, creating it")
            create_dir(directory_url=destination_directory,
                       keyfile_path=keyfile)

        destination_root_is_dir = check_if_directory_exists(directory_url=destination_root,
                                                            keyfile_path=keyfile)
        destination_root_is_file = check_if_file_exists(file_url=destination_root,
                                                        keyfile_path=keyfile)

        # delete old files in destination directory, if overwrite is set to true
        if overwrite:
            if destination_root_is_dir:
                logger.info(f"delete_old_files is set, deleting old files in {destination_root}")
                delete_directory(directory_url=destination_root,
                                 keyfile_path=keyfile)
            elif destination_root_is_file:
                logger.info(f"delete_old_files is set, deleting old file {destination_root}")
                delete_file(file_url=destination_root,
                            keyfile_path=keyfile)
            else:
                logger.warning(f"delete_old_files is set but destination directory or "
                               f"file {destination_root} does not exist, nothing to delete")

        # check, that, if source is a file, destination is also a file
        if source_is_file and not destination_root_is_file:
            raise ValueError(f"Source is a file, but destination is a directory")

        # get size of files to back up
        backup_size_bytes = get_size_of_files(source_directory,
                                              file_list=included_sub_nodes,
                                              keyfile_path=keyfile)

        backup_size_readable = bytes_to_readable(backup_size_bytes)
        backup_location_stats["size_on_source"] = backup_size_readable

        logger.info(f"Backing up {source_directory} to {destination_directory} ")
        logger.info(f"Size of files to back up: {backup_size_readable}")

        # sync directories
        rsync_directory(source_directory_url=source_directory,
                        destination_directory_url=destination_root,
                        keyfile_path=keyfile,
                        update_only=update_only,
                        delete_old_files=delete_old_files)

        # get final size of files in destination directory
        final_size_bytes = get_size_of_files(destination_root,
                                             file_list=included_sub_nodes,
                                             keyfile_path=keyfile)
        final_size_readable = bytes_to_readable(final_size_bytes)
        backup_location_stats["size_on_destination"] = final_size_readable

        logger.info(f"Size of files in destination directory: {final_size_readable}")

        current_end_time = datetime.now()

        time_elapsed_in_seconds = (current_end_time - current_start_time).seconds
        # format duration to hours, minutes, seconds
        backup_location_stats["backup_duration"] = f"{time_elapsed_in_seconds // 3600} hours, " \
                                                   f"{(time_elapsed_in_seconds // 60) % 60} minutes, " \
                                                   f"{time_elapsed_in_seconds % 60} seconds"
        # calculate average speed in megabytes per second
        backup_location_stats["average_speed"] = bytes_per_second_to_readable(backup_size_bytes,
                                                                              time_elapsed_in_seconds)

        if "name" in backup_location:
            location_name = backup_location["name"]
        else:
            # use parent directory as location name
            path_parts = source_directory.split("/")
            if path_parts[-1] == "":
                location_name = path_parts[-2]
            else:
                location_name = path_parts[-1]

        backup_stats[location_name] = backup_location_stats

    return backup_stats
