def parse_size_to_bytes(size_str: str) -> int:
    """
    Parse size string to bytes
    :param size_str: string containing size in bytes, kilobytes, megabytes, gigabytes or terabytes
    :return: size in bytes
    """

    if size_str.endswith('B'):
        size_str = size_str[:-1]
    if size_str.endswith('K'):
        size = float(size_str[:-1]) * 1000
    elif size_str.endswith('M'):
        size = float(size_str[:-1]) * 1000 * 1000
    elif size_str.endswith('G'):
        size = float(size_str[:-1]) * 1000 * 1000 * 1000
    elif size_str.endswith('T'):
        size = float(size_str[:-1]) * 1000 * 1000 * 1000 * 1000
    else:
        size = float(size_str)

    return int(size)


def bytes_to_readable(num_bytes: int) -> str:
    """
    Convert bytes to a human readable format
    :param num_bytes: number of bytes
    :return: human readable format
    """

    for unit in ['B', 'KB', 'MB', 'GB', 'TB']:
        if num_bytes < 1000.0:
            return f"{num_bytes:.2f} {unit}"
        num_bytes /= 1000.0
    return f"{num_bytes:.2f} PB"


def bytes_per_second_to_readable(bytes: int, seconds: int) -> str:
    """
    Convert bytes per second to a human readable format with KB/s, MB/s or GB/s
    :param bytes: number of bytes
    :param seconds: number of seconds
    :return: human readable format
    """

    bytes_per_second = bytes / seconds
    for unit in ['B/s', 'KB/s', 'MB/s', 'GB/s']:
        if bytes_per_second < 1000.0:
            return f"{bytes_per_second:.2f} {unit}"
        bytes_per_second /= 1000.0
    return f"{bytes_per_second:.2f} TB/s"
