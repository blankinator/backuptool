import os
import json


def load_config(config_file_path: str) -> dict:
    """
    Load config from file
    :param config_file_path: path to config file
    :return: dict with config
    """

    print(f"Loading config from {config_file_path}")

    try:
        with open(config_file_path, 'r') as f:
            config = json.load(f)
    except Exception as e:
        print(f"Error while loading config: {e}")
        raise e

    print(f"Config loaded successfully")

    return config


def get_backup_config(backup_config_name: str, config: dict) -> dict:
    """
    Get backup configuration from config dictionary
    :param backup_config_name: name of backup configuration
    :param config: configuration dict for backup tool
    :return: dict with backup configuration only
    """
    try:
        backup_configs = config['backups']
        backup_config = [backup for backup in backup_configs if backup['name'] == backup_config_name][0]
        return backup_config
    except KeyError as e:
        print(f'Error while reading backup config: {e}')
        raise e
