import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from typing import Callable

from backup_tool.utils.logging import get_logger

logger = get_logger(__name__)


def get_mail_config(config: dict) -> dict:
    """
    Get mail configuration from config dictionary
    :param config: configuration dict for backup tool
    :return: dict with mail configuration only
    """
    try:
        mail_config = config['notifications']['config']
        mail_config['recipients'] = config['notifications']['recipients']
        return mail_config
    except KeyError as e:
        logger.error(f'Error while reading config: {e}')
        raise e


def send_email(receiver_address: str,
               subject: str,
               content: str,
               smtp_server: str,
               smtp_port: int,
               username: str,
               password: str,
               sender_address: str) -> None:
    """
    Send email using SMTP_SSL
    :param receiver_address: address of recipient
    :param subject: subject of email
    :param content: content of email
    :param smtp_server: outgoing server address
    :param smtp_port: outgoing server port
    :param username: account name for sender email address
    :param password: password for sender email account
    :param sender_address: sender email address
    :return: None
    """
    try:
        message = MIMEMultipart()
        message['From'] = sender_address
        message['To'] = receiver_address
        message['Subject'] = subject
        message.attach(MIMEText(content, 'plain'))

        # Use SMTP_SSL for SSL
        session = smtplib.SMTP_SSL(smtp_server, smtp_port)
        session.login(username, password)
        text = message.as_string()
        session.sendmail(sender_address, receiver_address, text)
        session.quit()
        logger.info(f'Email sent successfully to {receiver_address}')
    except Exception as e:
        logger.error(f'Error while sending email: {e}')
        raise e


def send_notification(subject: str,
                      content: str,
                      recipient_addresses: list[str],
                      send_mail_function: Callable) -> None:
    """
    Send email notification using SMTP_SSL
    :param subject: subject of email
    :param content: content of email
    :param recipient_addresses: list of recipients for notification
    :param send_mail_function: function to send email
    :return: None
    """

    logger.info(f'Sending email notification to {recipient_addresses}, subject: {subject}, content: {content}')

    for recipient_address in recipient_addresses:
        send_mail_function(recipient_address, subject, content)
