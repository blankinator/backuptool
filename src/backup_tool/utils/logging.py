import logging


def get_logger(name: str, logfile_path: str = "./backup_log.log") -> logging.Logger:
    """
    Get logger instance
    :param name: name of logger
    :param logfile_path: path to logfile
    :return: logger instance
    """
    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)

    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    # add file handler
    fh = logging.FileHandler(logfile_path)
    fh.setLevel(logging.DEBUG)
    fh.setFormatter(formatter)
    logger.addHandler(fh)

    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    ch.setFormatter(formatter)
    logger.addHandler(ch)
    return logger
