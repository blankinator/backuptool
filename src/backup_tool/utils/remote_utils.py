import os
import subprocess
import json

from fabric import Connection

from utils.logging import get_logger

logger = get_logger(__name__)


def get_path_root(path: str) -> str:
    """
    Get the root of a given path
    :param path: path to get root from
    :return: root of path
    """
    path_parts = path.split("/")
    if path_parts[-1] == "":
        return path_parts[-2]
    else:
        return path_parts[-1]


def get_destination_root(destination_directory: str,
                         root_directory_name: str) -> str:
    """
    Get the root of the destination directory by combining the destination directory and the root directory name
    :param destination_directory: directory of the destination
    :param root_directory_name: name of the root directory
    :return: path of the root directory in the destination directory
    """

    path_parts = destination_directory.split("/")
    if path_parts[-1] == "":
        return f"{destination_directory}{root_directory_name}"
    else:
        return f"{destination_directory}/{root_directory_name}"


def is_remote_path(path: str) -> bool:
    """
    Check if a given path is a remote path
    :param path: path to check
    :return: True if path is remote, False otherwise
    """
    if "@" in path and ":" in path:
        return True
    if "@" in path and ":" not in path or "@" not in path and ":" in path:
        raise ValueError("Remote path must contain a colon (':') to separate host and path")
    return False


def check_if_directory_exists(directory_url: str,
                              keyfile_path: str = None) -> bool:
    """
    Check if a remote path exists
    :param directory_url: url of the directory to check, may be remote or local
    :param keyfile_path: path to keyfile for ssh connection
    :return: True if path exists, False otherwise
    """

    # check, if url is on remote server
    if is_remote_path(directory_url):
        remote_url, remote_path = directory_url.split(":")
        username = remote_url.split("@")[0]
        remote_url = remote_url.split("@")[1]

        # create connection to remote server
        with Connection(host=remote_url, user=username, connect_kwargs={"key_filename": keyfile_path}) as con:
            # check if path exists
            result = con.run(f"test -d {remote_path}", warn=True)

            return result.ok
    else:
        # assume local path
        return os.path.isdir(directory_url)


def check_if_file_exists(file_url: str,
                         keyfile_path: str = None) -> bool:
    """
    Check if a remote file exists and is not a directory
    :param file_url: url of the file to check, may be remote or local
    :param keyfile_path: path to keyfile for ssh connection
    :return: True if file exists, False otherwise
    """

    # check, if url is on remote server
    if is_remote_path(file_url):
        remote_url, remote_path = file_url.split(":")
        username = remote_url.split("@")[0]
        remote_url = remote_url.split("@")[1]

        # create connection to remote server
        with Connection(host=remote_url, user=username, connect_kwargs={"key_filename": keyfile_path}) as con:
            # check if file exists
            result = con.run(f"test -f {remote_path}", warn=True)

            return result.ok
    else:
        # assume local path
        return os.path.isfile(file_url)


def delete_file(file_url: str,
                keyfile_path: str = None) -> None:
    """
    Delete a file. If the file is a directory, raise an exception. File may be remote or local.
    :param file_url: url of the file to delete, may be remote or local
    :param keyfile_path: path to keyfile for ssh connection
    :return: None
    """

    # check, if url is on remote server
    if is_remote_path(file_url):
        remote_url, remote_path = file_url.split(":")
        username = remote_url.split("@")[0]
        remote_url = remote_url.split("@")[1]

        # create connection to remote server
        with Connection(host=remote_url, user=username, connect_kwargs={"key_filename": keyfile_path}) as con:
            # check if file exists
            result = con.run(f"test -f {remote_path}", warn=True)

            if not result.ok:
                raise FileNotFoundError(f"File {file_url} does not exist")

            # check if file is a directory
            result = con.run(f"test -d {remote_path}", warn=True)

            if result.ok:
                raise IsADirectoryError(f"File {file_url} is a directory")

            # delete file
            con.run(f"rm {remote_path}")

    else:
        # assume local path
        if not os.path.isfile(file_url):
            raise FileNotFoundError(f"File {file_url} does not exist")

        if os.path.isdir(file_url):
            raise IsADirectoryError(f"File {file_url} is a directory")

        os.remove(file_url)


def delete_directory(directory_url: str,
                     keyfile_path: str = None) -> None:
    """
    Delete a directory. If the directory is a file, raise an exception. Directory may be remote or local.
    :param directory_url: url of the directory to delete, may be remote or local
    :param keyfile_path: path to keyfile for ssh connection
    :return: None
    """

    # check, if url is on remote server
    if is_remote_path(directory_url):
        remote_url, remote_path = directory_url.split(":")
        username = remote_url.split("@")[0]
        remote_url = remote_url.split("@")[1]

        # create connection to remote server
        with Connection(host=remote_url, user=username, connect_kwargs={"key_filename": keyfile_path}) as con:
            # check if directory exists
            result = con.run(f"test -d {remote_path}", warn=True)

            if not result.ok:
                raise FileNotFoundError(f"Directory {directory_url} does not exist")

            # check if directory is a file
            result = con.run(f"test -f {remote_path}", warn=True)

            if result.ok:
                raise NotADirectoryError(f"Directory {directory_url} is a file")

            # delete directory
            con.run(f"rm -r {remote_path}")

    else:
        # assume local path
        if not os.path.isdir(directory_url):
            raise FileNotFoundError(f"Directory {directory_url} does not exist")

        if os.path.isfile(directory_url):
            raise NotADirectoryError(f"Directory {directory_url} is a file")

        # delete directory and all its content
        os.system(f"rm -r {directory_url}")


def get_size_of_files(url: str,
                      keyfile_path: str = None,
                      file_list: list = None) -> int:
    """
    Get size of content to back up
    :param url: url of the directory
    :param file_list: list of files to back up, if None, consider all files
    :param keyfile_path: path to keyfile for ssh connection
    :return: size of content in bytes
    """

    # check, if url is on remote server
    if is_remote_path(url):
        remote_url, remote_path = url.split(":")
        username = remote_url.split("@")[0]
        remote_url = remote_url.split("@")[1]

        # create connection to remote server
        if keyfile_path is None:
            con = Connection(host=remote_url, user=username)
        else:
            con = Connection(host=remote_url, user=username, connect_kwargs={"key_filename": keyfile_path})

        # get size of content
        if file_list:
            total_size = 0
            for file in file_list:
                result = con.run(f"du -sb {remote_path}/{file}", warn=True)
                total_size += int(result.stdout.split()[0])
        else:
            result = con.run(f"du -sb {remote_path}", warn=True)
            total_size = int(result.stdout.split()[0])

        con.close()

        return total_size
    else:
        # assume local path
        if file_list:
            total_size = 0
            for file in file_list:
                total_size += os.path.getsize(f"{url}/{file}")
        else:
            total_size = int(
                subprocess.run(f"du -sb {url}", shell=True, capture_output=True).stdout.split()[0].decode())

        return total_size


def create_dir(directory_url: str,
               keyfile_path: str,
               can_exists: bool = False) -> None:
    """
    Create a remote directory
    :param directory_url: url of the directory to create, may be remote or local
    :param keyfile_path: path to keyfile for ssh connection
    :param can_exists: set to True if directory can exist
    :return: None
    """

    # check if directory exists
    if check_if_directory_exists(directory_url, keyfile_path) and not can_exists:
        raise FileExistsError(f"Directory {directory_url} already exists")

    # check, if url is on remote server
    if is_remote_path(directory_url):
        remote_url, remote_path = directory_url.split(":")
        username = remote_url.split("@")[0]
        remote_url = remote_url.split("@")[1]

        # create connection to remote server
        with Connection(host=remote_url, user=username, connect_kwargs={"key_filename": keyfile_path}) as con:

            # create directory
            con.run(f"mkdir -p {remote_path}")
    else:
        # assume local path
        os.makedirs(directory_url, exist_ok=can_exists)


def get_directory_structure(directory_url: str,
                            keyfile_path: str = None) -> dict:
    """
    Get the structure of a directory as a nested dict
    :param directory_url: directory url to get structure from, may be remote or local
    :param keyfile_path: path to keyfile for ssh connection
    :return: nested dict with directory structure
    """

    tree_command_base = "tree -J -d --du "

    # check, if url is on remote server
    if is_remote_path(directory_url):
        remote_url, remote_path = directory_url.split(":")
        username = remote_url.split("@")[0]
        remote_url = remote_url.split("@")[1]

        # create connection to remote server
        with Connection(host=remote_url, user=username, connect_kwargs={"key_filename": keyfile_path}) as con:

            # check, if tree is installed
            result = con.run("which tree", warn=True)
            if not result.ok:
                raise FileNotFoundError("tree command not found on remote server")

            # create tree command
            tree_command = f"{tree_command_base} {remote_path}"

            # parse output to json
            result = con.run(tree_command, warn=True)
            structure = json.loads(result.stdout)
    else:
        # assume local path

        # check if tree is installed
        result = subprocess.run("which tree", shell=True, capture_output=True)
        if result.returncode != 0:
            raise FileNotFoundError("tree command not found on local machine")

        # create tree command
        tree_command = f"{tree_command_base} {directory_url}"
        result = subprocess.run(tree_command, shell=True, capture_output=True)
        structure = json.loads(result.stdout)

    return structure


def rsync_directory(source_directory_url: str,
                    destination_directory_url: str,
                    keyfile_path: str = None,
                    update_only: bool = False,
                    delete_old_files: bool = False) -> None:
    """
    Sync a directory from source to destination
    :param source_directory_url: source directory url
    :param destination_directory_url: destination directory url
    :param keyfile_path: path to keyfile for ssh connection
    :param update_only: set to True to update only
    :param delete_old_files: set to True to overwrite existing files
    :return: dict with sync stats
    """

    # check, if source and destination are on remote server
    source_is_remote = is_remote_path(source_directory_url)
    destination_is_remote = is_remote_path(destination_directory_url)

    if source_is_remote:
        source_remote_url, source_remote_path = source_directory_url.split(":")
        source_remote_url = source_remote_url.split("@")[1]
        source_url = f"{source_remote_url}:{source_remote_path}"
    else:
        source_url = source_directory_url

    if destination_is_remote:
        destination_remote_url, destination_remote_path = destination_directory_url.split(":")
        destination_remote_url = destination_remote_url.split("@")[1]
        destination_url = f"{destination_remote_url}:{destination_remote_path}"
    else:
        destination_url = destination_directory_url

    # create rsync command
    rsync_command = f"rsync --ignore-errors --info=progress2 -avz"
    if update_only:
        rsync_command += " --update"

    if delete_old_files:
        rsync_command += " --delete"

    if keyfile_path:
        rsync_command += f" -e 'ssh -i {keyfile_path}'"

    rsync_command += f" {source_url} {destination_url}"

    # run rsync command
    result = os.system(rsync_command)
    result_code = result >> 8
    # check if rsync command was successful
    if result_code == 23:
        # rsync error code 23: partial transfer due to error, mostly due to permission errors, ignore
        logger.warning(f"Partial transfer due to error while syncing directories: {rsync_command}")
    elif result_code != 0:
        raise Exception(f"Error while syncing directories: {rsync_command} with error code {result}")
